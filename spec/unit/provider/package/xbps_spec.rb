require 'spec_helper'
require 'puppet/provider/package/xbps'

describe Puppet::Type.type(:package).provider(:xbps) do
  let(:executor) { Puppet::Util::Execution }

  let(:resource) { Puppet::Type.type(:package).new(:name => 'package', :provider => 'xbps') }
  let(:resource_new) { Puppet::Type.type(:package).new(:name => 'package-new') }
  let(:provider) { described_class.new(resource) }

  describe 'when installing' do
    before do
      allow(provider).to receive(:query).and_return({
        :ensure => '1.0_1'
      })
    end

    it 'should call xbps-install to install the package' do
      expect(executor).to receive(:execute).
          ordered.
          with(['/usr/bin/xbps-install', ['-y', 'package']], anything).
          and_return('')

      provider.install
    end

    it 'should call xbps-query to list all resources during prefetch' do
      expect(executor).to receive(:execute).
        ordered.
        with(['/usr/bin/xbps-query', ['-l']], anything).
        and_return('[ii] package-new-1.0_1 Some package\n')

      expect(described_class.prefetch({'package-new' => resource_new})['package-new'].provider.properties).to eq({
        :ensure   => '1.0_1',
        :name     => 'package-new',
        :provider => :xbps,
        :version  => '1.0_1',
      })
    end
  end

  describe 'when uninstalling' do
    before do
      allow(provider).to receive(:query).and_return({
        :ensure => '1.0_1'
      })
    end

    it 'should call xbps-remove to uninstall the package' do
      expect(executor).to receive(:execute).
        ordered.
        with(['/usr/bin/xbps-remove', ['-y', 'package']], anything).
        and_return('')

      provider.uninstall
    end
  end

  describe 'when querying' do
    it 'should call xbps-query to query a single package' do
      expect(executor).to receive(:execute).
        ordered.
        with(['/usr/bin/xbps-query', ['-s', resource[:name] + '>=0']], anything).
        and_return('[ii] package-2.3_5 Some package\n')

      expect(provider.query).to eq({
        :ensure   => '2.3_5',
        :name     => 'package',
        :provider => :xbps,
        :version  => '2.3_5',
      })
    end

  end
end
