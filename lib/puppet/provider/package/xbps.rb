require 'puppet/provider/package'

Puppet::Type.type(:package).provide :xbps, :parent => Puppet::Provider::Package do
  desc "A xbps provider for Void Linux."

  confine :true => (Facter.value(:os).dig('distro', 'id') == 'VoidLinux')
  defaultfor :true => (Facter.value(:os).dig('distro', 'id') == 'VoidLinux')

  commands :xbps_install => "/usr/bin/xbps-install"
  commands :xbps_query   => "/usr/bin/xbps-query"
  commands :xbps_remove  => "/usr/bin/xbps-remove"

  has_feature :installable
  has_feature :uninstallable

  def self.parse_xbps_query(line)
    name, _, version = line.split(" ")[1].rpartition("-")

    {
      :ensure   => version,
      :name     => name,
      :provider => self.name,
      :version  => version,
    }
  end

  def self.instances
    packages = []

    output = xbps_query(["-l"])
    output.lines.each do |line|
      package = parse_xbps_query(line)
      packages << new(package)
    end

    packages
  end

  def self.prefetch(resources)
    packages = instances
    resources.each_key do |name|
      provider = packages.find{|p| p.name == name }
      if provider
        resources[name].provider = provider
      end
    end
  end

  def install
    xbps_install(['-y', resource[:name]])
  end

  def uninstall
    xbps_remove(['-y', resource[:name]])
  end

  def query
    begin
      output = xbps_query(["-s", resource[:name] + ">=0"])
    rescue Puppet::ExecutionFailure
      return nil
    end

    if output.lines.length == 0
      return nil
    end

    self.class.parse_xbps_query(output.lines[0])
  end
end
